Pulse Physiology Explorer
=========================

The Pulse Explorer is an application developed to provide a user interface on top of the the Pulse Physiology Engine API.
The Explorer allows you to :
  - Create and Run Patients
    - Apply chronic conditions to your patients 
  - Load/Create/Run Engine States
  - Interact with Pulse as it runs by adding actions
  - Interact with a virtual ventilator
  - View real time physiology data via a Vitals Monitor
  - Dive into more data by view live time series plots by requesting any data produced in Pulse
  - Save/Edit your actions into a scenario
  - Load and run any saved scenario
  - Create scenarios that save engine states for use in your application


## Build Environment

The code provided utilizes C++17, here is a list of popular compilers and their initial version to implement all of C++17 :
- GCC 7 and later
- Clang 5 and later
- MSVC 2017 and later

While the provided cmake superbuild automatically pulls many libraries it needs to compile, 
you will still need to have the following tools installed (along with your choice of C++ compiler) :

### CMake
Currently, the code requires CMake 3.12 or greater to properly.

Go to the cmake website, `https://cmake.org/download`, and download the appropriate distribution.

Debian Linux users may install via our <a href="https://apt.kitware.com/"> apt repository</a>.

Ensure that cmake bin is on your PATH and available in your cmd/bash shell.

### Qt

You will need to download and install Qt version 5 on your own.</br>
You can get an online installer from <a href="https://www.qt.io/download-open-source/">here</a>.

Run the installer and select 'Customize' to select version 5.15.2 to be installed.

(You can deselect the installation of Qt version 6 if it is enabled by default, you will only need 5.15.2)

### Java JDK

While there is no dependency on Java by the Explorer application, 
it is currently required by the engine to generate required data files.

Visit the <a href="https://gitlab.kitware.com/physiology/engine#java-jdk">Pulse ReadMe</a> for instructions on how to install the Java JDK on your system

## Building

The build is directed by CMake to ensure it can be built on various platforms. 
The code is build by a CMake 'superbuild', meaning as part of the build, CMake will download any
dependent libraries and compile those before it builds, specifically the Engine and ParaView.
The build is also out of source, meaning the code base is seperated from the build files.
This means you will need two folders, one for the source code and one for the build files.
Generally, I create a single directory to house these two folders.
Here is the quickest way to pull and build via a cmd/bash shell:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~bash
mkdir explorer
cd explorer
git clone https://gitlab.kitware.com/physiology/explorer src
mkdir builds
cd builds
# Feel free to make subfolders here, like msvc2017x64 or release or something
# Generate a make file/msvc solution for the external dependencies
# Note you need to provide cmake the source directory at the end (relative or absolute)
# Run CMake (it will use the system default compiler if you don't provide options or use the CMake GUI)
cmake -DCMAKE_BUILD_TYPE:STRING=Release ../src
# Note you will need to provide the path to Qt if it is not on the PATH
cmake -DCMAKE_BUILD_TYPE:STRING=Release -DQt5_DIR=C:/Programming/Tools/Qt/5.15.2.1/msvc2017_64/lib/cmake/Qt5 ../src
# If you have already build Pulse, you can also specify it to CMake
# The <path/to/Pulse/build/install> is the install directory in your build that contains the PulseConfig.cmake file
cmake -DCMAKE_BUILD_TYPE:STRING=Release -DQt5_DIR=C:/Programming/Tools/Qt/5.15.2.1/msvc2017_64/lib/cmake/Qt5 -DPulse_DIR=<path/to/Pulse/build/install> ../src

# Build the install target/project
# On Linux/OSX/MinGW 
make  
# For MSVC
# Open the OuterBuild.sln and build the project (It will build everything!)
# When the build is complete, MSVC users can close the OuterBuild solution, and open the PhysiologyExplorer.sln located in the PhysiologyExplorer directory.
# Unix based systems should also cd into this directory for building any changes to the PhysiologyExplorer code base
cd PhysiologyExplorer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The application will be build in the <build dir>/install

## Notes

### Running a build on Linux

Inorder to run the Explorer you will need to set the QT_QPA_PLATFORM_PLUGIN_PATH variable in the PulseExplorer.sh file to your QT plugins directory.

Start the explorer using this script.

!! The Explorer is still under development and may be unstable !!

### Qt ini

As the GUI is based on Qt, if the GUI is missing any widgets, such as the graphing views,
you will need to remove the Qt PhysiologyExplorer0.1.0.ini file from your system.

It can be found in your user directory ./configs
Or on windows:
C:\Users\your_login\AppData\Roaming\Kitware, Inc

If you find any other issues, please do not hesitate to log any issue in our repository.





